package com.epam.rd.java.basic.task7.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionManager {

  public static Connection getConnection(String url, String user, String password)
      throws DBException {
    try {
      return DriverManager.getConnection(url, user, password);
    } catch (SQLException e) {
      throw new DBException("Can't connect to database!", e);
    }
  }

  public static Statement getStatement(Connection connection) throws DBException{
    try {
      return connection.createStatement();
    } catch (SQLException e) {
      throw new DBException("Can't create statement", e);
    }
  }

}
