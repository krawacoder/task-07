package com.epam.rd.java.basic.task7.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";
	private static final String DB_USER = "postgres";
	private static final String DB_PASSWORD = "krawaa";

	public static synchronized DBManager getInstance() {
		instance = new DBManager();
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		Connection connection =  ConnectionManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
		Statement statement = ConnectionManager.getStatement(connection);

		String SQL_SELECT_QUERY = "SELECT * FROM users";
		ResultSet resultSet = null;
		try {
			resultSet = statement.executeQuery(SQL_SELECT_QUERY);
		} catch (SQLException e) {
			throw new DBException("SQL query execution problem!", e);
		}

		List<User> userList = new ArrayList<>();
		try {
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String login = resultSet.getString("login");
				User user = new User();
				user.setId(id);
				user.setLogin(login);
				userList.add(user);
			}
			statement.close();
			connection.close();
		} catch (SQLException e) {
			throw new DBException("Something wrong", e);
		}

		return userList;
	}

	public boolean insertUser(User user) throws DBException {
		Connection connection =  ConnectionManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
		Statement statement = ConnectionManager.getStatement(connection);

		String login = user.getLogin();
		String SQL_INSERT_QUERY = "INSERT INTO users (login) VALUES ('" + login + "')";
		try {
			statement.executeUpdate(SQL_INSERT_QUERY);
			statement.close();
			connection.close();
			return true;
		} catch (SQLException e) {
			throw new DBException("Insertion problem", e);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		return false;
	}

	public User getUser(String login) throws DBException{
		Connection connection =  ConnectionManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
		Statement statement = ConnectionManager.getStatement(connection);

		String SQL_SELECT_QUERY = "SELECT * FROM users WHERE login='" + login + "'";
		try {
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_QUERY);
			if (resultSet.next()) {
				int id = resultSet.getInt("id");
				String userLogin = resultSet.getString("login");

				User user = new User();
				user.setId(id);
				user.setLogin(userLogin);

				statement.close();
				connection.close();
				return user;
			} else {
				statement.close();
				connection.close();
				return null;
			}
		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	public Team getTeam(String name) throws DBException{
		Connection connection =  ConnectionManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
		Statement statement = ConnectionManager.getStatement(connection);

		String SQL_SELECT_QUERY = "SELECT * FROM teams WHERE name='" + name + "'";
		try {
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_QUERY);
			if (resultSet.next()) {
				int id = resultSet.getInt("id");
				String teamName = resultSet.getString("name");
				Team team = new Team();
				team.setId(id);
				team.setName(teamName);

				statement.close();
				connection.close();
				return team;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DBException("", e);
		}

	}

	public List<Team> findAllTeams() throws DBException {
		Connection connection =  ConnectionManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
		Statement statement = ConnectionManager.getStatement(connection);

		String SQL_SELECT_QUERY = "SELECT * FROM teams";
		try {
			ResultSet	resultSet = statement.executeQuery(SQL_SELECT_QUERY);
			List<Team> teamListList = new ArrayList<>();
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				Team team = new Team();
				team.setId(id);
				team.setName(name);
				teamListList.add(team);
			}

			statement.close();
			connection.close();
			return teamListList;
		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection connection =  ConnectionManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
		Statement statement = ConnectionManager.getStatement(connection);

		String teamname = team.getName();
		String SQL_INSERT_QUERY = "INSERT INTO teams (name) VALUES ('" + teamname + "')";
		try {
			statement.executeUpdate(SQL_INSERT_QUERY);

			statement.close();
			connection.close();
			return true;
		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		int userID = user.getId();
		int[] teamsID = new int[teams.length];
		for (int i = 0; i < teamsID.length; i++) {
			teamsID[i] = teams[i].getId();
		}

		Connection connection =  ConnectionManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
		try {
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			throw new DBException("", e);
		}
		Statement statement = ConnectionManager.getStatement(connection);

		try	{
			for (int j : teamsID) {
				String SQL_INSERT_QUERY = "INSERT INTO users_teams VALUES (" + userID + ", " + j + ")";
				statement.executeUpdate(SQL_INSERT_QUERY);
			}
			connection.commit();
			statement.close();
			connection.close();
			return true;
		} catch (SQLException e) {
			throw new DBException("Can't insert values into users_teams table", e);
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		Connection connection =  ConnectionManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
		Statement statement = ConnectionManager.getStatement(connection);

		String SQL_SELECT_QUERY = "SELECT id, name FROM teams INNER JOIN users_teams"
				+ " ON users_teams.team_id = teams.id WHERE user_id=" + user.getId();
		List<Team> teamsList = new ArrayList<>();

		try {
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_QUERY);

			while (resultSet.next()) {
				int team_id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				Team team = new Team();
				team.setId(team_id);
				team.setName(name);
				teamsList.add(team);
			}

			return teamsList;
		} catch (SQLException e) {
			throw new DBException("", e);
		}

	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection connection = ConnectionManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
		Statement statement = ConnectionManager.getStatement(connection);

		String SQL_DELETE_QUERY = "DELETE FROM teams WHERE name='" + team.getName() + "'";
		try {
			statement.executeQuery(SQL_DELETE_QUERY);
			return true;
		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		return false;
	}

}
